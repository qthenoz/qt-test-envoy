# Reste à faire

- Créer une interface qui défini la structure du fichier envoy.blade.php (implémentation spécifique par typo de projet)
    - créer répo git avec branche de base + une branche par typo de projet (wp, drupal 8, ...)
- (optionnel) checker automatiquement les modifs sur composer.lock, /assets, autre ? (envoi d'un .rev ?) 
    - dans la task "status" : checker si il y a des mises à jour
- documenter l'utilisation d'envoy