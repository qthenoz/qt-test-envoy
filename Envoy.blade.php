{{-- TODO: add ssh key to the box --}}
@setup
    require __DIR__.'/vendor/autoload.php';
    
    $env = $env ? $env : 'staging';
    
    $file = '.envoy/config--' . $env . '.yml';
    if (!file_exists($file)) {
        throw new Exception('ERROR: config file "' . $file . '" does not exist!');
        exit;
    }

    $yamlContents = Symfony\Component\Yaml\Yaml::parse(file_get_contents($file));

    extract($yamlContents);
    $skip_git = $no_git ? true : false;
    $skip_composer = $no_composer ? true : false;
    $skip_npm = $no_npm ? true : false;

    $datetime = new \DateTime('now');
    $datetime = $datetime->format('Ymd-His');

    // assert that master is pulled on prod
    if ($env == "prod" && (isset($branch) && $branch != "master")) {
        throw new Exception('ERROR: you can not deploy another branch than master on the production environment!');
        exit;
    }
@endsetup

@import('.envoy/functions.blade.php')

@servers(['web' => [$SERVER]])

@story('deploy', ['on' => 'web'])
    hook_init
    hook_git_before
    git
    hook_git_after
    hook_composer_before
    composer
    hook_composer_after
    hook_npm_before
    npm
    hook_npm_after
    hook_complete
@endstory

{{-- =========== --}}
{{-- Basic tasks --}}
{{-- =========== --}}

@task('git')
    @if (!$skip_git)
        cd {{ $ROOT_PATH }}

        echo "Git pull..."
        @if ($branch)
            git pull origin {{ $branch }}
        @else
            git pull origin master
        @endif
    @else
        echo "git skipped"
    @endif
@endtask


@task('composer')
    @if ($USE_COMPOSER && !$skip_composer)
        echo "Install vendors"
        cd {{ $APP_PATH }}
        echo "php {{ $COMPOSER_PATH }} install"
        php {{ $COMPOSER_PATH }} install
    @else
        echo "composer skipped"
    @endif
@endtask

@task('npm')
    @if ($USE_NPM && !$skip_npm)
        cd {{ $NPM_PATH }}

        echo "run npm"
        npm install
        
        echo "run build"
        npm run prod

        echo "remove node_modules"
        rm -rf node_modules
    @else
        echo "NPM skipped"
    @endif
@endtask

{{-- ============= --}}
{{-- Utility tasks --}}
{{-- ============= --}}

@task('status', [$SERVER])
    {{-- TODO: This as config --}}
    cd {{ $ROOT_PATH }}
    
    echo 'Current branch';
    git branch

    echo 'Commit label';
    git log -1 --pretty=%B
    
    echo 'Hash:'
    git rev-parse HEAD
@endtask