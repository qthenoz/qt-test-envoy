This is a test repository only, it aims to test the deployment method with Envoy. A production-ready repository will be up soon.

# Envoy

Global documentation here: https://laravel.com/docs/master/envoy

## Prepare remote (first use only)

On an empty vhost:

- `git init`
- `git remote add origin <repository>`
- `git pull origin <branch>`

Make sure that DocumentRoot path is correct on your vhost configuration.

Setup your application (database, images, ...)

## Setup local environment

On your box : 
- `composer global require laravel/envoy`

Edit `.envoy/config--<env_name>.yml` and make sure that paths are correct. You may also choose not to use composer or npm here

Make sure that your `package.json` file declares a `prod` script that will build your stuff.

## Deploy with Envoy

Basic usage: 
`envoy run deploy <options>`

Options available:
- `--env=<env_name>` the target environment (default = staging), note that a config file named accordingly SHOULD exist 
- `--branch=<branch_name>` the branch to be pulled (default = master)
- `--no_npm` skip the npm part
- `--no_composer` skip the composer part

Get status from remote:
`envoy run status`

## Extend Envoy

You can use hooks that are presents in `.envoy/functions.blade.php` to add custom functionalities such as dumping the database, setting maintenance mode, ect...

While writing those hooks, you will have access to all variables set in `.envoy/config--<env_name>.yml` and also
- `$datetime` : the current datetime in the format "Ymd-His"
- `$branch` : the name of the branch passed with `--branch`, if any
- `$env` : the name of the target environment (default = staging)

# Scotch Box Pro

Just a Dead-Simple Local LAMP/LEMP Stack for Vagrant. **Making Vagrant EASY AS EATING CAKE for developers.**

![Scotch Box](https://box.scotch.io/img/pro-banner.png)

Scotch Box is a pre-configured Vagrant Box with a full array of features to get you up and running with Vagrant in no time.

NGINX version: https://github.com/scotch-io/scotch-box-pro-nginx


## License Required?

This is a paid version of the original Scotch Box. [Go Pro Now!](https://box.scotch.io/pro) to get access to documentation and new tutorials!


## Pro Features

* NEW OS: **Ubuntu-17.10!**
* NEW PHP: **PHP 7.2!**
* NEW APACHE: 2.4.29
* NEW NGINX: 1.13.8
* NEW RUBY via RVM: 2.5.0
* NEW NODE via NVM: 8.9.4
* NEW BUILD SCRIPTS
* Fixes a MongoDB and PHP bug
* **Makes Laravel routing finally work out of the box with NGINX version**
* Adds Drush (Launcher) even though you should do this through Composer these days
* Updated WP-CLI version
* Generally WAY higher versions of everything else


![Scotch Box](https://box.scotch.io/img/terminal.png)

## Documentation

* Check out the official docs at: [box.scotch.io](https://box.scotch.io)
* [Read the getting started article](https://scotch.io/bar-talk/introducing-scotch-box-a-vagrant-lamp-stack-that-just-works)
* [Read the 2.0 release article](https://scotch.io/bar-talk/announcing-scotch-box-2-0-our-dead-simple-vagrant-lamp-stack-improved)
* [Read the 2.5 release article](https://scotch.io/bar-talk/announcing-scotch-box-2-5)
* [Read the 3.0 release article](https://scotch.io/bar-talk/announcing-scotch-box-30-and-scotch-box-pro)
* [Read the 3.5 release article](https://scotch.io/bar-talk/announcing-scotch-box-v35-and-scotch-box-pro-v15-the-big-switcheroo)


## More Information

Check-out [box.scotch.io](https://box.scotch.io) to learn more.
